#include <stdio.h>

int main() {
  double nilai, quiz, absen, uts, uas, tugas;
  char Huruf_Mutu;

  // Mengubah nilai default dengan input user
  printf("Masukkan nilai absen: ");
  scanf("%lf", &absen);
  printf("Masukkan nilai tugas: ");
  scanf("%lf", &tugas);
  printf("Masukkan nilai quiz: ");
  scanf("%lf", &quiz);
  printf("Masukkan nilai UTS: ");
  scanf("%lf", &uts);
  printf("Masukkan nilai UAS: ");
  scanf("%lf", &uas);

  // Mengubah persentase nilai
  double persentase_absen, persentase_tugas, persentase_quiz, persentase_uts, persentase_uas;
  printf("Masukkan persentase absen: ");
  scanf("%lf", &persentase_absen);
  printf("Masukkan persentase tugas: ");
  scanf("%lf", &persentase_tugas);
  printf("Masukkan persentase quiz: ");
  scanf("%lf", &persentase_quiz);
  printf("Masukkan persentase UTS: ");
  scanf("%lf", &persentase_uts);
  printf("Masukkan persentase UAS: ");
  scanf("%lf", &persentase_uas);

  // Menghitung nilai akhir
  nilai = ((persentase_absen * absen) + (persentase_tugas * tugas) + (persentase_quiz * quiz) + (persentase_uts * uts) + (persentase_uas * uas)) / 100;

  // Menentukan huruf mutu
  if (nilai > 85 && nilai <= 100)
    Huruf_Mutu = 'A';
  else if (nilai > 70 && nilai <= 85)
    Huruf_Mutu = 'B';
  else if (nilai > 55 && nilai <= 70)
    Huruf_Mutu = 'C';
  else if (nilai > 40 && nilai <= 55)
    Huruf_Mutu = 'D';
  else if (nilai >= 0 && nilai <= 40)
    Huruf_Mutu = 'E';

  // Menampilkan hasil
  printf("Nilai akhir: %.2f\n", nilai);
  printf("Huruf Mutu: %c\n", Huruf_Mutu);

  return 0;
}
